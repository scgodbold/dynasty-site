package main

import (
    "time"
    "log"
    "strconv"

    "gitlab.com/scgodbold/dynasty-site/fleas"
    "gitlab.com/scgodbold/dynasty-site/db"

    "github.com/google/uuid"
)

type Updater struct {
    UpdateFrequency time.Duration
}

func createChampion(year int, champId int) error {
    owner, err := store.GetOwnerById(champId)
    if err != nil {
        return err
    }
    season, err := store.GetSeasonByYear(year)
    if err != nil {
        return err
    }
    champ := db.Champion{
        Owner: owner,
        Season: season,
    }
    return store.CreateChampion(&champ)

}
func updatePointTotal(team *db.Owner, pFor float32, pAgainst float32) error {
    if pTotal, err := store.GetPointTotalByOwnerId(team.Id); err != nil {
        // Create the total
        pTotal := db.PointTotal{
            Id: uuid.New().String(),
            Owner: team,
            PointsFor: pFor,
            PointsAgainst: pAgainst,
        }
        return store.CreatePointTotal(&pTotal)
    } else {
        // Update the total
        pTotal.PointsFor = pTotal.PointsFor + pFor
        pTotal.PointsAgainst = pTotal.PointsAgainst + pAgainst
        return store.UpdatePointTotal(pTotal)
    }
}

func updateRecord(team *db.Owner, wins int, loses int, ties int) error {
    if record, err := store.GetRecordByOwnerId(team.Id); err != nil {
        record := db.Record{
            Id: uuid.New().String(),
            Owner: team,
            Wins: wins,
            Loses: loses,
            Ties: ties,
        }
        err := store.CreateRecord(&record)
        if err != nil {
            return err
        }
    } else {
        record.Wins = record.Wins + wins
        record.Loses = record.Loses + loses
        record.Ties = record.Ties + ties
        return store.UpdateRecord(record)
    }
    return nil
}

func updateRecords(game *db.Game) error {
    tie := 0
    homeWin := 0
    awayWin := 0
    if game.HomeScore == game.AwayScore {
        tie = 1
    } else if game.HomeScore > game.AwayScore {
        homeWin = 1
    } else if game.AwayScore > game.HomeScore {
        awayWin = 1
    }

    if err := updateRecord(game.HomeTeam, homeWin, awayWin, tie); err != nil {
        return err
    }
    if err := updateRecord(game.AwayTeam, awayWin, homeWin, tie); err != nil {
        return err
    }
    return nil
}

func updateOwner(id int, name string) (db.Owner, error) {
    team := db.Owner{
        Id: id,
        Name: name,
    }

    if owner, err := store.GetOwnerById(team.Id); err != nil {
        return team, store.CreateOwner(&team)
    } else {
        if name != owner.Name {
            store.UpdateOwner(&team)
        }
    }
    return team, nil
}

func processWeek(year int, week int) {
    games, err := store.GetGamesByWeek(week, year)
    if err == nil && len(games) > 0 {
        // We have this data, no need to bother FF
        return
    }
    weekData, err := fleas.FetchWeek(year, LEAGUEID, week)
    if err != nil {
        log.Fatal(err.Error())
    }

    for _, match := range weekData.Games {
        if !match.Final {
            return
        }

        // Update Owner
        home, err := updateOwner(match.HomeTeam.Id, match.HomeTeam.Name)
        if err != nil {
            log.Fatal(err.Error())
        }
        away, err := updateOwner(match.AwayTeam.Id, match.AwayTeam.Name)
        if err != nil {
            log.Fatal(err.Error())
        }

        s, err := store.GetSeasonByYear(year)
        if err != nil {
            log.Fatal(err.Error())
        }

        gameId, err := strconv.Atoi(match.Id)
        if err != nil {
            log.Fatal(err.Error())
        }

        game := db.Game{
            Id: gameId,
            Season: s,
            Week: week,
            HomeTeam: &home,
            AwayTeam: &away,
            HomeScore: match.HomeScore.Score.Value,
            AwayScore: match.AwayScore.Score.Value,
            RegularSeason: !match.Playoffs,
        }
        if !store.GameExists(game.Id) {
            store.CreateGame(&game)
            if err := updateRecords(&game); err != nil {
                log.Fatal(err.Error())
            }
            if err := updatePointTotal(game.HomeTeam, game.HomeScore, game.AwayScore); err != nil {
                log.Fatal(err.Error())
            }
            if err := updatePointTotal(game.AwayTeam, game.AwayScore, game.HomeScore); err != nil {
                log.Fatal(err.Error())
            }
            if match.Championship {
                if match.HomeScore.Score.Value > match.AwayScore.Score.Value {
                    createChampion(year, match.HomeTeam.Id)
                } else {
                    createChampion(year, match.AwayTeam.Id)
                }
            }
        }
    }

}

func processWeeks(year int) {
    w, err := fleas.FetchWeek(year, LEAGUEID, 1)
    now := time.Now()
    nowMilli := int(now.UnixNano() / 1000000)
    if err != nil  {
        log.Fatal(err.Error())
    }
    for _, week := range w.EligibleSchedulePeriods {
        weekStart, err := strconv.Atoi(week.Time.StartEpoch)
        if err != nil {
            log.Fatal(err.Error())
        }
        if weekStart > nowMilli {
            break
        }
        processWeek(year, week.Week)
    }
}


func processYears() {
    var year = INITIALYEAR
    var now = time.Now()
    for {
        if year > now.Year() {
            break
        }
        if _, err := store.GetSeasonByYear(year); err != nil {
            s := db.Season {Id: uuid.New().String(), Year: year}
            err:= store.CreateSeason(&s)
            if err != nil {
                log.Fatal(err.Error())
            }
        }
        processWeeks(year)
        year += 1
    }
}

func processDivisionsYear(year int) {
    season, err := store.GetSeasonByYear(year)
    if err != nil {
        log.Fatal(err.Error())
    }
    for name := range conf.Divisions[year] {
        if !store.DivisionExists(season, name) {
            division := &db.Division{
                Id: uuid.New().String(),
                Name: name,
                Season: season,
            }
            if err := store.CreateDivision(division); err != nil {
                log.Fatal(err.Error())
            }
            for _, member := range conf.Divisions[year][name] {
                owner, err := store.GetOwnerById(member)
                if err != nil {
                    log.Fatal(err.Error())
                }
                divisionMember := &db.DivisionMember{
                    Division: division,
                    Owner: owner,
                }
                if err := store.CreateDivisionMember(divisionMember); err != nil {
                    log.Fatal(err.Error())
                }
            }
        }
    }
}

func processDivisions() {
    for year := range conf.Divisions {
        processDivisionsYear(year)
    }
}

func (u *Updater) Run() {
    for {
        log.Println("Running updates")
        processYears()
        processDivisions()
        store.UpdateOrCreateMetadata()
        log.Printf("Waiting %v before checking for updates\n", u.UpdateFrequency)
        time.Sleep(u.UpdateFrequency)
    }
}
