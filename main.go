package main

// TODO: Look at this https://www.legendsleagueff.com/resources

import (
    "fmt"
    "sort"
    "log"
    "time"
    "html/template"
    "net/http"
    "database/sql"

    "gitlab.com/scgodbold/dynasty-site/db"
    "gitlab.com/scgodbold/dynasty-site/config"

    "github.com/pressly/goose/v3"
    "github.com/gorilla/mux"
    _ "github.com/lib/pq"
)

const INITIALYEAR = 2016
const LEAGUEID = "186030"

var store db.Store
var conf *config.Config

func render(target string) (*template.Template, error){
    files := []string{
        target,
        "./templates/partials/banner.html.tmpl",
        "./templates/partials/footer.html.tmpl",
        "./templates/partials/news.html.tmpl",
        "./templates/partials/nav.html.tmpl",
        "./templates/partials/champion-card.html.tmpl",
        "./templates/partials/division-standings.html.tmpl",
        "./templates/partials/allTimeWins.html.tmpl",
        "./templates/layouts/base.html.tmpl",
    }

    return template.ParseFiles(files...)
}

func home(w http.ResponseWriter, r *http.Request) {

    tmpl, err := render("./templates/layouts/home.html.tmpl")
    now := time.Now()

    type payload struct {
        Champion *db.Champion
        DivisionStandings map[string][]RegularSeasonRecord
        AllTimeWins []*db.Record
        UpdatedAt time.Time
    }

    data := payload{}

    if err != nil {
        log.Println(err.Error())
        http.Error(w, "Internal Server Error", 500)
        return
    }
    champ, err := store.GetChampionByYear(now.Year() - 1)
    data.Champion = champ
    if err != nil {
        log.Println(err.Error())
        http.Error(w, "Internal Server Error", 500)
        return
    }

    standings, err := GenerateDivisionRecords(now.Year())
    if err != nil {
        log.Println(err.Error())
        http.Error(w, "Internal Server Error", 500)
        return
    }
    data.DivisionStandings = standings

    allTimeWs, err := store.GetRecords()
    if err != nil {
        log.Println(err.Error())
        http.Error(w, "Internal Server Error", 500)
        return
    }
    sort.Sort(sort.Reverse(db.ByAllTimeWins(allTimeWs)))
    data.AllTimeWins = allTimeWs

    metadata, err := store.GetMetaData()
    if err != nil {
        log.Println(err.Error())
        http.Error(w, "Internal Server Error", 500)
        return
    }
    data.UpdatedAt = metadata.Updated

    err = tmpl.Execute(w, data)

    if err != nil {
        log.Println(err.Error())
        http.Error(w, "Internal Server Error", 500)
    }
}

func healthcheck(w http.ResponseWriter, r *http.Request) {
    w.Write([]byte("OK"))
}

func init() {
    log.Println("Starting Configuration")
    conf = config.InitConfig()
    log.Println("Connecting to Database")
    database, err := sql.Open("postgres", conf.DbConnectionString)
    if err != nil {
        log.Fatal(err.Error())
    }
    store = db.InitStore(database)

    // Handle Migrations
    log.Println("Handling Migrations")
    if err := goose.Up(database, "db/migrations"); err != nil {
        panic(err)
    }
}

func main() {
    defer store.Close()
    // processYears()
    // processDivisions()

    r := mux.NewRouter()

    go func() {
        u := Updater{UpdateFrequency: conf.UpdateFrequency}
        u.Run()
    }()

    r.HandleFunc("/", home)
    r.HandleFunc("/healthcheck", healthcheck)
    r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("./static"))))
    log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", conf.Port), r))
}
