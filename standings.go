package main

import (
    "sort"
    "log"
    "net/http"

    "gitlab.com/scgodbold/dynasty-site/db"
)

// Standings determined by
// 1. Wins
// 2. Less Losses
// 3. Head 2 Head Record
// 4. Points For Avg
// 5. Opponenet Points

type RegularSeasonRecord struct {
    Owner *db.Owner
    Wins int
    Loses int
    Ties int
    Games int
    PointsFor float32
    OpponentPoints float32
    DivisionRecords map[int]*db.Record
}

type ByRegularSeasonRecord []RegularSeasonRecord

func (b ByRegularSeasonRecord) Len() int { return len(b) }
func (b ByRegularSeasonRecord) Less(i, j int) bool {
    // Check 1: Wins
    if b[i].Wins < b[j].Wins { return true }
    if b[i].Wins > b[j].Wins { return false }

    // Check 2: Less Losses
    if b[i].Loses > b[j].Loses { return true }
    if b[i].Loses < b[j].Loses { return false }

    // Check 3: head 2 head
    record := b[i].DivisionRecords[b[j].Owner.Id]
    if record.Wins < record.Loses { return true}
    if record.Wins > record.Loses { return false}

    // Check 4: PointsFor
    if b[i].PointsFor < b[j].PointsFor { return true }
    if b[i].PointsFor > b[j].PointsFor { return false }

    // Check 5: OpponentPoints
    if b[i].OpponentPoints < b[j].OpponentPoints { return true }
    if b[i].OpponentPoints > b[j].OpponentPoints { return false }

    return false
}
func (b ByRegularSeasonRecord) Swap(i, j int) { b[i], b[j] = b[j], b[i] }


func GenerateDivisionRecord (members []*db.DivisionMember) ([]RegularSeasonRecord, error) {
    records := []RegularSeasonRecord{}
    for _, member := range members {
        record, err := store.GetRegularSeasonRecordByOwner(member.Owner, member.Division.Season)
        if err != nil {
            return records, err
        }
        pointsFor, err := store.GetRegularSeasonPointsByOwner(member.Owner, member.Division.Season)
        if err != nil {
            return records, err
        }
        pointsAgainst, err := store.GetRegularSeasonOppositionPoints(member.Owner, member.Division.Season)
        if err != nil {
            return records, err
        }
        r := RegularSeasonRecord {
            Owner: member.Owner,
            Wins: record.Wins,
            Loses: record.Loses,
            Ties: record.Ties,
            PointsFor: pointsFor,
            OpponentPoints: pointsAgainst,
            DivisionRecords: map[int]*db.Record{},

        }
        for _, m := range members {
            if member.Owner.Id == m.Owner.Id {
                // Skip self
                continue
            }
            oppRecord, err := store.GetRegularSeasonHeadToHeadRecord(member.Owner, m.Owner, member.Division.Season)
            if err != nil {
                return records, err
            }
            r.DivisionRecords[m.Owner.Id] = oppRecord

        }
        records = append(records, r)
    }
    sort.Sort(sort.Reverse(ByRegularSeasonRecord(records)))
    return records, nil
}

func GenerateDivisionRecords(year int) (map[string][]RegularSeasonRecord, error) {
    records := map[string][]RegularSeasonRecord{}
    season, err := store.GetSeasonByYear(year)
    if err != nil {
        return records, err
    }
    divisions, err :=  store.GetDivisionsBySeason(season)
    if err != nil {
        return records, err
    }

    for _, division := range divisions {
        divisionMembers, err := store.GetDivisionMembers(division)
        if err != nil {
            return records, err
        }

        r, err := GenerateDivisionRecord(divisionMembers)
        if err != nil {
            return records, err
        }
        records[division.Name] = r
    }
    return records, nil
}

func standings(w http.ResponseWriter, r *http.Request) {
    tmpl, err := render("./templates/layouts/home.html.tmpl")

    if err != nil {
        log.Println(err.Error())
        http.Error(w, "Internal Server Error", 500)
        return
    }

    err = tmpl.Execute(w, nil)
    if err != nil {
        log.Println(err.Error())
        http.Error(w, "Internal Server Error", 500)
    }
}
