.PHONY: build

build:
	go build

run:
	./dynasty-site

migration-status:
	${GOPATH}/bin/goose -dir ./db/migrations postgres "${DB_CONNECTION_STRING}" status

migration-up:
	${GOPATH}/bin/goose -dir ./db/migrations postgres "${DB_CONNECTION_STRING}" up

migration-down:
	${GOPATH}/bin/goose -dir ./db/migrations postgres "${DB_CONNECTION_STRING}" down

migration-up-one:
	${GOPATH}/bin/goose -dir ./db/migrations postgres "${DB_CONNECTION_STRING}" up-by-one

migration-down-one:
	${GOPATH}/bin/goose -dir ./db/migrations postgres "${DB_CONNECTION_STRING}" down-by-one

migration-redo:
	${GOPATH}/bin/goose -dir ./db/migrations postgres "${DB_CONNECTION_STRING}" redo

dev-db-down:
	docker-compose down

dev-db-up:
	docker-compose up -d

test-docker:
	docker build -t local:dynasty-site .
	docker run -e PORT=8080 -e DS_DBCONN_STRING="${DB_CONNECTION_STRING}" -it local:dynasty-site
