package db

import (
    "fmt"
)

type Season struct {
    Id string
    Year int
}

func (store *dbStore) CreateSeason(season *Season) error {
    rows, err := store.db.Query("INSERT INTO seasons(id, year) VALUES ($1, $2)", season.Id, season.Year)
    if err != nil {
        fmt.Println("creating")
        return err
    }
    rows.Close()
    return nil
}

func (store *dbStore) GetSeasonByYear(year int) (*Season, error) {
    rows, err := store.db.Query("SELECT * FROM seasons WHERE year = $1", year)
    if err != nil {
        return nil, err
    }
    season := &Season{}
    rows.Next()
    if err := rows.Scan(&season.Id, &season.Year); err != nil {
        rows.Close()
        return nil, err
    }
    rows.Close()
    return season, nil
}

func (store *dbStore) GetSeasons() ([]*Season, error) {
    rows, err := store.db.Query("SELECT * FROM seasons")
    if err != nil {
        return nil, err
    }

    seasons := []*Season{}
    for rows.Next() {
        season := &Season{}
        if err := rows.Scan(&season.Id, &season.Year); err != nil {
            rows.Close()
            return nil, err
        }
        seasons = append(seasons, season)
    }
    rows.Close()
    return seasons, nil
}

func (store *dbStore) GetSeasonById(id string) (*Season, error) {
    rows, err := store.db.Query("SELECT * FROM seasons WHERE year = $1", id)
    if err != nil {
        return nil, err
    }
    season := &Season{}
    rows.Next()
    if err := rows.Scan(&season.Id, &season.Year); err != nil {
        rows.Close()
        return nil, err
    }
    rows.Close()
    return season, nil
}
