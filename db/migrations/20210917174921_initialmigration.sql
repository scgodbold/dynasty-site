-- +goose Up
-- +goose StatementBegin
CREATE TABLE seasons (
    id varchar(36) not null,
    year int not null,
    PRIMARY KEY (id)
);
-- +goose StatementEnd
-- +goose StatementBegin
CREATE TABLE owners (
    id int not null,
    name varchar(2048) not null,
    PRIMARY KEY (id)
);
-- +goose StatementEnd
-- +goose StatementBegin
CREATE TABLE games (
    id int not null,
    season varchar(36) not null,
    week int not null,
    homeTeam int not null,
    awayTeam int not null,
    homeScore decimal(5,2),
    awayScore decimal(5,2),
    regularSeason boolean,
    PRIMARY KEY (id),
    FOREIGN KEY (homeTeam) REFERENCES owners(id),
    FOREIGN KEY (awayTeam) REFERENCES owners(id),
    FOREIGN KEY (season) REFERENCES seasons(id)
);
-- +goose StatementEnd
-- +goose StatementBegin
CREATE TABLE records (
    id varchar(36) not null,
    owner int not null,
    wins int,
    loses int,
    ties int,
    PRIMARY KEY (id),
    FOREIGN KEY (owner) REFERENCES owners(id),
    UNIQUE (owner)

);
-- +goose StatementEnd
-- +goose StatementBegin
CREATE TABLE pointTotals (
    id varchar(36) not null,
    owner int not null,
    pointsFor decimal(18,2),
    pointsAgainst decimal(18,2),
    PRIMARY KEY (id),
    FOREIGN KEY (owner) REFERENCES owners(id),
    UNIQUE (owner)
);
-- +goose StatementEnd
-- +goose StatementBegin
CREATE TABLE champions (
    owner int not null,
    season varchar(36) not null,
    PRIMARY KEY (season),
    FOREIGN KEY (owner) REFERENCES owners(id),
    FOREIGN KEY (season) REFERENCES seasons(id)
);
-- +goose StatementEnd
-- +goose StatementBegin
CREATE TABLE divisions (
    id varchar(36) not null,
    name varchar(512) not null,
    season varchar(36) not null,
    PRIMARY KEY (id),
    FOREIGN KEY (season) REFERENCES seasons(id)
);
-- +goose StatementEnd
-- +goose StatementBegin
CREATE TABLE divisionMembers (
    division varchar(36) not null,
    owner int not null,
    FOREIGN KEY (division) REFERENCES divisions(id),
    FOREIGN KEY (owner) REFERENCES owners(id)
);
-- +goose StatementEnd
-- +goose StatementBegin
CREATE TABLE metadata (
    id varchar(36) not null,
    updated timestamp
)
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE metadata
-- +goose StatementEnd
-- +goose StatementBegin
DROP TABLE divisionMembers;
-- +goose StatementEnd
-- +goose StatementBegin
DROP TABLE divisions;
-- +goose StatementEnd
-- +goose StatementBegin
DROP TABLE champions;
-- +goose StatementEnd
-- +goose StatementBegin
DROP TABLE pointTotals;
-- +goose StatementEnd
-- +goose StatementBegin
DROP TABLE records;
-- +goose StatementEnd
-- +goose StatementBegin
DROP TABLE games;
-- +goose StatementEnd
-- +goose StatementBegin
DROP TABLE owners;
-- +goose StatementEnd
-- +goose StatementBegin
DROP TABLE seasons;
-- +goose StatementEnd
