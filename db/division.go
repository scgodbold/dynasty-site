package db

type Division struct {
    Id string
    Name string
    Season *Season
}

type DivisionMember struct {
    Division *Division
    Owner *Owner
}

func (store *dbStore) CreateDivision(division *Division) error {
    rows, err := store.db.Query(
        "INSERT INTO divisions (id, name, season) VALUES ($1, $2, $3)",
        division.Id,
        division.Name,
        division.Season.Id,
    )
    if err != nil {
        return err
    }
    defer rows.Close()
    return nil
}

func (store *dbStore) CreateDivisionMember(divisionMember *DivisionMember) error {
    rows, err := store.db.Query(
        "INSERT INTO divisionMembers (division, owner) VALUES ($1, $2)",
        divisionMember.Division.Id,
        divisionMember.Owner.Id,
    )
    if err != nil {
        return err
    }
    defer rows.Close()
    return nil
}

func (store *dbStore) GetDivisionMembers(division *Division) ([]*DivisionMember, error) {
    rows, err := store.db.Query(
        "SELECT owner from divisionMembers WHERE division = $1",
        division.Id,
    )
    if err != nil {
        return nil, err
    }
    defer rows.Close()
    members := []*DivisionMember{}
    for rows.Next() {
        var ownerId int
        if err := rows.Scan(&ownerId); err != nil {
            return nil, err
        }
        owner, err := store.GetOwnerById(ownerId)
        if err != nil {
            return nil, err
        }
        member := &DivisionMember{
            Division: division,
            Owner: owner,
        }
        members = append(members, member)
    }
    return members, nil
}

func (store *dbStore) GetDivisionsBySeason(season *Season) ([]*Division, error) {
    rows, err := store.db.Query(
        "SELECT id, name FROM divisions WHERE season = $1",
        season.Id,
    )
    if err != nil {
        return nil, err
    }
    defer rows.Close()
    divisions := []*Division{}
    for rows.Next() {
        division := &Division{}
        if err := rows.Scan(&division.Id, &division.Name); err != nil {
            return nil, err
        }
        division.Season = season
        divisions = append(divisions, division)
    }
    return divisions, nil
}

func (store *dbStore) DivisionExists(season *Season, name string) bool {
    rows, err := store.db.Query(
        "SELECT id FROM divisions WHERE season = $1 AND name = $2",
        season.Id,
        name,
    )
    defer rows.Close()
    if err != nil {
        return false
    }
    var id string
    rows.Next()
    if err := rows.Scan(&id); err != nil {
        return false
    }
    return id != ""
}
