package db

import (
    "time"
    "github.com/google/uuid"
)

type Metadata struct {
    Id string
    Updated time.Time
}

func (store *dbStore) CreateMetaData(meta *Metadata) error {
    rows, err := store.db.Query(
        "INSERT INTO metadata(id, updated) VALUES ($1, $2)",
        meta.Id,
        meta.Updated,
    )
    if err != nil {
        return err
    }
    rows.Close()
    return nil
}

func (store *dbStore) GetMetaData() (*Metadata, error) {
    meta := &Metadata{}
    rows, err := store.db.Query(
        "SELECT * FROM metadata LIMIT 1",
    )
    if err != nil {
        return meta, err
    }
    defer rows.Close()
    rows.Next()
    if err := rows.Scan(&meta.Id, &meta.Updated); err != nil {
        return meta, err
    }
    return meta, nil
}

func (store *dbStore) UpdateOrCreateMetadata() error {
    meta, err := store.GetMetaData()
    if err != nil {
        meta = &Metadata{
            Id: uuid.New().String(),
            Updated: time.Now(),
        }
        return store.CreateMetaData(meta)
    } else {
        meta.Updated = time.Now()
        rows, err := store.db.Query(
            "UPDATE metadata SET updated = $1 WHERE id = $2",
            meta.Updated,
            meta.Id,
        )
        if err != nil {
            return err
        }
        defer rows.Close()
    }
    return nil
}
