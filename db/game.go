package db

type Game struct {
    Id int
    Season *Season
    Week int
    HomeTeam *Owner
    AwayTeam *Owner
    HomeScore float32
    AwayScore float32
    RegularSeason bool
}

func (store *dbStore) CreateGame(game *Game) error {
    rows, err := store.db.Query(
        "INSERT INTO games (id, season, week, homeTeam, awayTeam, homeScore, awayScore, regularSeason) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)",
        game.Id,
        game.Season.Id,
        game.Week,
        game.HomeTeam.Id,
        game.AwayTeam.Id,
        game.HomeScore,
        game.AwayScore,
        game.RegularSeason,
    )
    if err != nil {
        return err
    }
    rows.Close()
    return nil
}

func (store *dbStore) GetGameById(id int) (*Game, error) {
    rows, err := store.db.Query("SELECT * FROM games WHERE id = $1", id)
    if err != nil {
        return nil, err
    }

    rows.Next()

    game := &Game{}
    var seasonId string
    var homeId int
    var awayId int
    if err := rows.Scan(&game.Id, &seasonId, &game.Week, &homeId, &awayId, &game.HomeScore, &game.AwayScore, &game.RegularSeason); err != nil {
        rows.Close()
        return nil, err
    }
    rows.Close()

    homeTeam, err := store.GetOwnerById(homeId)
    if err != nil {
        return nil, err
    }
    awayTeam, err := store.GetOwnerById(awayId)
    if err != nil {
        return nil, err
    }
    season, err := store.GetSeasonById(seasonId)
    if err != nil {
        return nil, err
    }
    game.Season = season
    game.HomeTeam = homeTeam
    game.AwayTeam = awayTeam
    return game, nil
}

func (store *dbStore) GameExists(id int) bool {
    rows, err := store.db.Query("SELECT * FROM games WHERE id = $1", id)
    if err != nil {
        return false
    }
    if rows.Next() {
        rows.Close()
        return true
    }
    rows.Close()
    return false
}

func (store *dbStore) GetGamesByWeek(week int, year int) ([]*Game, error) {
    season, err := store.GetSeasonByYear(year)
    if err != nil {
        return nil, err
    }

    rows, err := store.db.Query("SELECT id, week, homeTeam, awayTeam, homeScore, awayScore, regularSeason FROM games WHERE week = $1 AND season = $2", week, season.Id)
    if err != nil {
        return nil, err
    }
    defer rows.Close()
    games := []*Game{}
    for rows.Next() {
        game := &Game{}
        var homeId int
        var awayId int
        if err := rows.Scan(&game.Id, &game.Week, &homeId, &awayId, &game.HomeScore, &game.AwayScore, &game.RegularSeason); err != nil {
            return nil, err
        }
        homeTeam, err := store.GetOwnerById(homeId)
        if err != nil {
            return nil, err
        }
        awayTeam, err := store.GetOwnerById(awayId)
        if err != nil {
            return nil, err
        }
        game.HomeTeam = homeTeam
        game.AwayTeam = awayTeam
        game.Season = season
        games = append(games, game)
    }
    return games, nil
}

func (store *dbStore) GetGamesByYear(year int) ([]*Game, error) {
    season, err := store.GetSeasonByYear(year)
    if err != nil {
        return nil, err
    }

    rows, err := store.db.Query("SELECT id, week, homeTeam, awayTeam, homeScore, awayScore, regularSeason FROM games WHERE season = $1", season.Id)
    if err != nil {
        return nil, err
    }
    defer rows.Close()
    games := []*Game{}
    for rows.Next() {
        game := &Game{}
        var homeId int
        var awayId int
        if err := rows.Scan(&game.Id, &game.Week, &homeId, &awayId, &game.HomeScore, &game.AwayScore, &game.RegularSeason); err != nil {
            return nil, err
        }
        homeTeam, err := store.GetOwnerById(homeId)
        if err != nil {
            return nil, err
        }
        awayTeam, err := store.GetOwnerById(awayId)
        if err != nil {
            return nil, err
        }
        game.HomeTeam = homeTeam
        game.AwayTeam = awayTeam
        game.Season = season
        games = append(games, game)
    }
    return games, nil
}

func (store *dbStore) GetRegularSeasonRecordByOwner(owner *Owner, season *Season) (*Record, error) {
    r := &Record{Id: "", Owner: owner, Wins:0, Loses:0, Ties: 0}
    rows, err := store.db.Query(
        "SELECT homeTeam, homeScore, awayScore FROM games WHERE season = $1 AND (homeTeam = $2 OR awayTeam = $2) AND regularSeason = True",
        season.Id,
        owner.Id,

    )
    if err != nil {
        return r, err
    }
    defer rows.Close()
    for rows.Next() {
        var homeId int
        var homeScore, awayScore float32
        if err := rows.Scan(&homeId, &homeScore, &awayScore); err != nil {
            return r, err
        }

        if owner.Id == homeId {
            if homeScore > awayScore {
                r.Wins = r.Wins + 1
            } else if homeScore < awayScore {
                r.Loses = r.Loses + 1
            } else {
                r.Ties = r.Ties + 1
            }
        } else {
            if awayScore > homeScore {
                r.Wins = r.Wins + 1
            } else if awayScore < homeScore {
                r.Loses = r.Loses + 1
            } else {
                r.Ties = r.Ties + 1
            }
        }
    }
    return r, nil
}

func (store *dbStore) GetRegularSeasonPointsByOwner(owner *Owner, season *Season) (float32, error) {
    points := float32(0.0)
    rows, err := store.db.Query(
        "SELECT homeTeam, homeScore, awayScore FROM games WHERE season = $1 AND (homeTeam = $2 OR awayTeam = $2) AND regularSeason = True",
        season.Id,
        owner.Id,
    )
    if err != nil {
        return points, err
    }
    defer rows.Close()

    for rows.Next() {
        var homeId int
        var homeScore, awayScore float32
        if err := rows.Scan(&homeId, &homeScore, &awayScore); err != nil {
            return points, err
        }
        if owner.Id == homeId {
            points = points + homeScore
        } else {
            points = points + awayScore
        }
    }

    return points, nil

}

func (store *dbStore) GetRegularSeasonOppositionPoints(owner *Owner, season *Season) (float32, error) {
    oppPoints := float32(0.0)
    rows, err :=  store.db.Query(
        "SELECT homeTeam, homeScore, awayScore FROM games WHERE season = $1 AND regularSeason = True AND (homeTeam = $2 OR awayTeam = $2)",
        season.Id,
        owner.Id,
    )
    if err != nil {
        return oppPoints, err
    }
    defer rows.Close()
    for rows.Next() {
        var homeId int
        var homeScore, awayScore float32
        if err := rows.Scan(&homeId, &homeScore, &awayScore); err != nil {
            return oppPoints, err
        }
        if owner.Id == homeId {
            oppPoints = oppPoints + awayScore
        } else {
            oppPoints = oppPoints + homeScore
        }
    }
    return oppPoints, nil
}

func (store *dbStore) GetRegularSeasonHeadToHeadRecord(owner *Owner, opposition *Owner, season *Season) (*Record, error) {
    r := &Record{Id: "", Owner: owner, Wins:0, Loses:0, Ties: 0}
    rows, err := store.db.Query(
        "SELECT homeTeam, homeScore, awayScore FROM games WHERE season = $1 AND regularSeason = True AND ((homeTeam = $2 AND awayTeam = $3) OR (homeTeam = $3 AND awayTeam = $2))",
        season.Id,
        owner.Id,
        opposition.Id,
    )
    if err != nil {
        return r, err
    }
    defer rows.Close()

    for rows.Next() {
        var homeId int
        var homeScore, awayScore float32
        if err := rows.Scan(&homeId, &homeScore, &awayScore); err != nil {
            return r, err
        }
        if homeId == owner.Id {
            if homeScore > awayScore {
                r.Wins = r.Wins + 1
            } else if homeScore < awayScore {
                r.Loses = r.Loses + 1
            } else {
                r.Ties = r.Ties + 1
            }
        } else {
            if homeScore > awayScore {
                r.Loses = r.Loses + 1
            } else if homeScore < awayScore {
                r.Wins = r.Wins + 1
            } else {
                r.Ties = r.Ties + 1
            }
        }
    }
    return r, nil
}
