package db

type Owner struct {
    Id int
    Name string
    Games []*Game
}

type ByRecord []*Owner

func (b ByRecord) Len() int { return len(b) }
func (b ByRecord) Swap(i, j int) { b[i], b[j] = b[j], b[i] }
func (b ByRecord) Less(i, j int) {

}

func (store *dbStore) CreateOwner(owner *Owner) error {
    rows, err := store.db.Query("INSERT INTO owners(id, name) VALUES ($1, $2)", owner.Id, owner.Name)
    if err != nil {
        return err
    }
    rows.Close()
    return nil
}

func (store *dbStore) GetOwnerById(id int) (*Owner, error) {
    rows, err := store.db.Query("SELECT * FROM owners WHERE id = $1", id)
    if err != nil {
        return nil, err
    }

    owner := &Owner{}
    rows.Next()
    if err := rows.Scan(&owner.Id, &owner.Name); err != nil {
        rows.Close()
        return nil, err
    }

    rows.Close()
    return owner, nil
}

func (store *dbStore) UpdateOwner(owner *Owner) error {
    rows, err := store.db.Query("UPDATE owners SET name = $1 WHERE id = $2", owner.Name, owner.Id)
    if err != nil {
        return err
    }
    rows.Close()
    return nil
}

func (store *dbStore) OwnerExists(id int) bool {
    rows, err := store.db.Query("SELECT * FROM owners WHERE id = $1", id)
    if err != nil {
        return false
    }

    if rows.Next() {
        rows.Close()
        return true
    }
    rows.Close()
    return false
}
