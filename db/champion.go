package db

type Champion struct {
    Owner *Owner
    Season *Season
}

func (store *dbStore) CreateChampion(champ *Champion) error {
    rows, err := store.db.Query(
        "INSERT INTO champions (owner, season) VALUES ($1, $2)",
        champ.Owner.Id,
        champ.Season.Id,
    )
    if err != nil {
        return err
    }
    defer rows.Close()
    return nil
}

func (store *dbStore) GetChampionshipsByOwner(ownerId int) ([]*Champion, error) {
    rows, err := store.db.Query(
        "SELECT season FROM champions WHERE owner = $1",
        ownerId,
    )
    if err != nil {
        return nil, err
    }
    defer rows.Close()
    owner, err := store.GetOwnerById(ownerId)
    if err != nil {
        return nil, err
    }
    championships := []*Champion{}
    for rows.Next() {
        var seasonId string
        if err := rows.Scan(&seasonId); err != nil {
            return nil, err
        }
        season, err := store.GetSeasonById(seasonId)
        if err != nil {
            return nil, err
        }
        championship := &Champion{
            Owner: owner,
            Season: season,
        }
        championships = append(championships, championship)
    }
    return championships, nil
}

func (store *dbStore) GetChampionByYear(year int) (*Champion, error){
    season, err := store.GetSeasonByYear(year)
    if err != nil {
        return nil, err
    }
    rows, err := store.db.Query(
        "SELECT owner FROM champions WHERE season = $1",
        season.Id,
    )
    if err != nil {
        return nil, err
    }
    rows.Next()
    var ownerId int
    if err := rows.Scan(&ownerId); err != nil {
        return nil, err
    }
    owner, err := store.GetOwnerById(ownerId)
    if err != nil {
        return nil, err
    }
    return &Champion{
        Owner: owner,
        Season: season,
    }, nil
}
