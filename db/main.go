package db

import(
    "database/sql"
)

type storeError struct {
    msg string
}

func (s *storeError) Error() string {
    return s.msg
}

func newStoreError(msg string) error {
    return &storeError {
        msg: msg,
    }
}

type Store interface {
    // Season Functions
    CreateSeason(season *Season) error
    GetSeasonByYear(year int) (*Season, error)
    GetSeasons() ([]*Season, error)
    GetSeasonById(id string) (*Season, error)

    // Owner Functions
    CreateOwner(owner *Owner) error
    GetOwnerById(id int) (*Owner, error)
    UpdateOwner(owner *Owner) error
    OwnerExists(id int) bool

    // Game Functions
    CreateGame(game *Game) error
    GetGameById(id int) (*Game, error)
    GameExists(id int) bool
    GetGamesByWeek(week int, year int) ([]*Game, error)
    GetGamesByYear(year int) ([]*Game, error)
    GetRegularSeasonRecordByOwner(owner *Owner, season *Season) (*Record, error)
    GetRegularSeasonPointsByOwner(owner *Owner, season *Season) (float32, error)
    GetRegularSeasonOppositionPoints(owner *Owner, season *Season) (float32, error)
    GetRegularSeasonHeadToHeadRecord(owner *Owner, opposition *Owner, season *Season) (*Record, error)

    // Record Functions
    CreateRecord(record *Record) error
    GetRecordByOwnerId(ownerId int) (*Record, error)
    UpdateRecord(record *Record) error
    GetRecords() ([]*Record, error)

    // PointTotal Functions
    CreatePointTotal(pointTotal *PointTotal) error
    GetPointTotalByOwnerId(ownerId int) (*PointTotal, error)
    UpdatePointTotal(PointTotal *PointTotal) error

    // Champion Functions
    CreateChampion(champ *Champion) error
    GetChampionByYear(year int) (*Champion, error)
    GetChampionshipsByOwner(ownerId int) ([]*Champion, error)

    // Division Functions
    CreateDivision(division *Division) error
    CreateDivisionMember(divisionMember *DivisionMember) error
    GetDivisionMembers(division *Division) ([]*DivisionMember, error)
    GetDivisionsBySeason(season *Season) ([]*Division, error)
    DivisionExists(season *Season, name string) bool

    // Metadata
    CreateMetaData(meta *Metadata) error
    GetMetaData() (*Metadata, error)
    UpdateOrCreateMetadata() error

    // Cleanup
    Close()
}

type dbStore struct {
    db *sql.DB
}

func (store *dbStore) Close() {
    store.db.Close()
}

func InitStore(db *sql.DB) Store {
    return &dbStore{db: db}
}
