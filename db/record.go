package db

import "fmt"

type Record struct {
    Id string
    Owner *Owner
    Wins int
    Loses int
    Ties int
}

func (r *Record) WinPercentage() float32 {
    return float32(r.Wins) / float32(r.Wins + r.Loses + r.Ties)
}

func (r *Record) TotalGames() int {
    return r.Wins + r.Loses + r.Ties
}

type ByAllTimeWins []*Record
func (a ByAllTimeWins) Len()  int { return len(a) }
func (a ByAllTimeWins) Less(i, j int) bool { return a[i].Wins < a[j].Wins }
func (a ByAllTimeWins) Swap(i, j int) { a[i], a[j] = a[j], a[i] }

type ByAllTimeWinPercentage []*Record
func (a ByAllTimeWinPercentage) Len()  int { return len(a) }
func (a ByAllTimeWinPercentage) Less(i, j int) bool { return a[i].WinPercentage() < a[j].WinPercentage() }
func (a ByAllTimeWinPercentage) Swap(i, j int) { a[i], a[j] = a[j], a[i] }


func (store *dbStore) CreateRecord(record *Record) error {
    rows, err := store.db.Query(
        "INSERT INTO records (id, owner, wins, loses, ties) VALUES ($1, $2, $3, $4, $5)",
        record.Id,
        record.Owner.Id,
        record.Wins,
        record.Loses,
        record.Ties,
    )
    if err != nil {
        return err
    }
    defer rows.Close()
    return nil
}

func (store *dbStore) GetRecordByOwnerId(ownerId int) (*Record, error) {
    rows, err := store.db.Query(
        "SELECT id, wins, loses, ties FROM records WHERE owner = $1",
        ownerId,
    )
    if err != nil {
        return nil, err
    }
    defer rows.Close()
    record := &Record{}
    rows.Next()
    if err := rows.Scan(&record.Id, &record.Wins, &record.Loses, &record.Ties); err != nil {
        return nil, err
    }
    owner, err := store.GetOwnerById(ownerId)
    if err != nil {
        return nil, err
    }
    record.Owner = owner
    if record.Id == "" {
        return nil, newStoreError(fmt.Sprintf("Got no results for: %v", ownerId))
    }
    return record, nil
}

func (store *dbStore) UpdateRecord(record *Record) error {
    rows, err := store.db.Query(
        "UPDATE records SET wins = $1, loses = $2, ties = $3 WHERE owner = $4",
        record.Wins,
        record.Loses,
        record.Ties,
        record.Owner.Id,
    )
    if err != nil {
        return err
    }
    defer rows.Close()
    return nil
}

func (store *dbStore) GetRecords() ([]*Record, error) {
    records := []*Record{}
    rows, err := store.db.Query(
        "SELECT * FROM records",
    )
    if err != nil {
        return records, err
    }
    defer rows.Close()

    for rows.Next() {
        var ownerId int
        record := &Record{}
        if err := rows.Scan(&record.Id, &ownerId, &record.Wins, &record.Loses, &record.Ties); err != nil {
            return nil, err
        }
        owner, err := store.GetOwnerById(ownerId)
        if err != nil {
            return nil, err
        }
        record.Owner = owner
        records = append(records, record)
    }
    return records, nil
}
