package db

import (
    "fmt"
)

type PointTotal struct {
    Id string
    Owner *Owner
    PointsFor float32
    PointsAgainst float32
}

func (store *dbStore) CreatePointTotal(pointTotal *PointTotal) error {
    rows, err := store.db.Query(
        "INSERT INTO pointTotals (id, owner, pointsFor, pointsAgainst) VALUES ($1, $2, $3, $4)",
        pointTotal.Id,
        pointTotal.Owner.Id,
        pointTotal.PointsFor,
        pointTotal.PointsAgainst,
    )
    if err != nil {
        return err
    }
    defer rows.Close()
    return nil
}

func (store *dbStore) GetPointTotalByOwnerId(ownerId int) (*PointTotal, error) {
    rows, err := store.db.Query(
        "SELECT id, pointsFor, pointsAgainst FROM pointTotals WHERE owner = $1",
        ownerId,
    )
    if err != nil {
        return nil, err
    }
    defer rows.Close()
    pointTotal := &PointTotal{}
    rows.Next()
    if err := rows.Scan(&pointTotal.Id, &pointTotal.PointsFor, &pointTotal.PointsAgainst); err != nil {
        return nil, err
    }
    pointTotal.Owner, err = store.GetOwnerById(ownerId)
    if err != nil {
        return nil, err
    }
    if pointTotal.Id == "" {
        return nil, newStoreError(fmt.Sprintf("No PointTotal found for: %v\n", ownerId))
    }
    return pointTotal, nil
}

func (store *dbStore) UpdatePointTotal(pointTotal *PointTotal) error {
    rows, err := store.db.Query(
        "UPDATE pointTotals SET pointsFor = $1, pointsAgainst = $2 WHERE owner = $3",
        pointTotal.PointsFor,
        pointTotal.PointsAgainst,
        pointTotal.Owner.Id,
    )
    if err != nil {
        return err
    }
    defer rows.Close()
    return nil
}
