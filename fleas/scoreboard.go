package fleas

import (
    "fmt"
    "encoding/json"
    "io/ioutil"
    "net/http"
)

type SchedulePeriod struct {
    Week int `json:"ordinal"`
    StartEpoch string `json:"startEpochMilli"`
    Now bool `json:"isNow"`
}

type SchedulePeriodRange struct {
    Week int `json:"ordinal"`
    Time SchedulePeriod `json:"low"`
    Now bool `json:"containsNow"`
}

type Score struct {
    Value float32
}

type GameScore struct {
    Score Score
}

type Team struct {
    Id int
    Name string
}

type Game struct {
    Id string
    Final bool `json:"isFinalScore"`
    Playoffs bool `json:"isPlayoffs"`
    Championship bool `json:"isChampionshipGame"`
    ThirdPlace bool `json:"isThirdPlaceGame"`
    HomeScore GameScore `json:"homeScore"`
    AwayScore GameScore `json:"awayScore"`
    HomeTeam Team `json:"home"`
    AwayTeam Team `json:"away"`
}

type Scoreboard struct {
    SchedulePeriod SchedulePeriodRange `json:"schedulePeriod"`
    EligibleSchedulePeriods []SchedulePeriodRange `json:"eligibleSchedulePeriods"`
    Games []Game
}


func FetchWeek(year int, leagueId string, week int) (Scoreboard, error) {
    url := fmt.Sprintf("https://www.fleaflicker.com/api/FetchLeagueScoreboard?sport=NFL&league_id=%v&season=%v&scoring_period=%v",leagueId, year, week)
    resp, err := http.Get(url)

    var scoreboard Scoreboard

    if err != nil {
        return scoreboard, err
    }

    defer resp.Body.Close()

    body, err := ioutil.ReadAll(resp.Body)

    err = json.Unmarshal(body, &scoreboard)

    return scoreboard, err
}
