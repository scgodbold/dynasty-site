package config

import (
    "log"
    "os"
    "time"
)

type Config struct {
    DbConnectionString string
    UpdateFrequency time.Duration
    Port string
    Divisions map[int]map[string][]int
}

func InitConfig() *Config {
    port := os.Getenv("DS_PORT")
    if port == "" {
        port = "8000"
    }

    freq, err := time.ParseDuration(os.Getenv("DS_UPDATE_FREQUENCY"))
    if err != nil {
        log.Printf("Error processing update frequency: %v\n", err.Error())
        freq = 60 * time.Second
    }
    config := &Config{}
    config.DbConnectionString = os.Getenv("DS_DBCONN_STRING")
    config.Port = port
    config.UpdateFrequency = freq

    addDivisions(config)

    return config
}

func addDivisions(config *Config) {
    config.Divisions = map[int]map[string][]int{}
    // 2020 Divisions
    config.Divisions[2020] = map[string][]int{}
    config.Divisions[2020]["North"] = []int{1261647, 1261632, 1263655}
    config.Divisions[2020]["East"] = []int{1262209, 1261603, 1261603}
    config.Divisions[2020]["South"] = []int{1261694, 1261694, 1261694}
    config.Divisions[2020]["West"] = []int{1262113, 1262360, 1262768}

    // 2021 Divisions
    config.Divisions[2021] = map[string][]int{}
    config.Divisions[2021]["North"] = []int{1261641, 1262113, 1263030}
    config.Divisions[2021]["East"] = []int{1262360, 1261603, 1261632}
    config.Divisions[2021]["South"] = []int{1261694, 1263655, 1262768}
    config.Divisions[2021]["West"] = []int{1262209, 1261647, 1262093}
}
